﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiimoteWPF
{
    public class WiimoteInputReport
    {
        #region Constants
        // Byte Value that represents each buton
        private const byte aButton = 0x08;
        private const byte bButton = 0x04;
        private const byte upButton = 0x08;
        private const byte downButton = 0x04;
        private const byte leftButton = 0x01;
        private const byte rightButton = 0x02;
        private const byte oneButton = 0x02;
        private const byte twoButton = 0x01;
        private const byte minusButton = 0x10;
        private const byte plusButton = 0x10;
        private const byte homeButton = 0x80;

        // Index of the button data in the full byte array received from the controller
        private const byte aIndex = 2;
        private const byte bIndex = 2;
        private const byte upIndex = 1;
        private const byte downIndex = 1;
        private const byte leftIndex = 1;
        private const byte rightIndex = 1;
        private const byte oneIndex = 2;
        private const byte twoIndex = 2;
        private const byte minusIndex = 2;
        private const byte plusIndex = 1;
        private const byte homeIndex = 2;
        #endregion

        #region Private Members
        private List<GamePadButtonFlags> flagList = new List<GamePadButtonFlags>();
        private AccelerometerInfo accelInfo = new AccelerometerInfo();
        #endregion

        #region Properties
        public float[] RawData { get; private set; } = new float[22];
        public AccelerometerInfo AccelInfo
        {
            get
            {
                return accelInfo;
            }
        }

        #endregion

        #region Constructor
        public WiimoteInputReport(byte[] buffer, AccelCalibrationInfo accelCalibrationInfo)
        {
            RawData = buffer.Select(b => (float)Convert.ToDouble(b)).ToArray();

            accelInfo.X = (float)(buffer[3] - accelCalibrationInfo.X0) / (accelCalibrationInfo.XG - accelCalibrationInfo.X0);
            accelInfo.Y = (float)(buffer[4] - accelCalibrationInfo.Y0) / (accelCalibrationInfo.YG - accelCalibrationInfo.Y0);
            accelInfo.Z = (float)(buffer[5] - accelCalibrationInfo.Z0) / (accelCalibrationInfo.ZG - accelCalibrationInfo.Z0);

            // Set Flags for the A button
            if ((buffer[aIndex] & aButton) != 0)
                flagList.Add(GamePadButtonFlags.A);

            if ((buffer[bIndex] & bButton) != 0)
                flagList.Add(GamePadButtonFlags.B);

            if ((buffer[upIndex] & upButton) != 0)
                flagList.Add(GamePadButtonFlags.Up);

            if ((buffer[downIndex] & downButton) != 0)
                flagList.Add(GamePadButtonFlags.Down);

            if ((buffer[leftIndex] & leftButton) != 0)
                flagList.Add(GamePadButtonFlags.Left);

            if ((buffer[rightIndex] & rightButton) != 0)
                flagList.Add(GamePadButtonFlags.Right);

            if ((buffer[oneIndex] & oneButton) != 0)
                flagList.Add(GamePadButtonFlags.One);

            if ((buffer[twoIndex] & twoButton) != 0)
                flagList.Add(GamePadButtonFlags.Two);

            if ((buffer[minusIndex] & minusButton) != 0)
                flagList.Add(GamePadButtonFlags.Minus);

            if ((buffer[plusIndex] & plusButton) != 0)
                flagList.Add(GamePadButtonFlags.Plus);

            if ((buffer[homeIndex] & homeButton) != 0)
                flagList.Add(GamePadButtonFlags.Home);
        }
        #endregion

        #region Public Methods
        public bool HasFlags(GamePadButtonFlags flags)
        {
            return flagList.Contains(flags);
        }
        #endregion
    }
}
