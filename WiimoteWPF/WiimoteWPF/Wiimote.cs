﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.HumanInterfaceDevice;

namespace WiimoteWPF
{
    class Wiimote
    {
        #region DLL Imports
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern SafeFileHandle CreateFile(string lpFileName, uint dwDesiredAccess,
          uint dwShareMode, IntPtr lpSecurityAttributes, uint dwCreationDisposition,
          uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern SafeFileHandle CreateFile2(string lpFileName, uint dwDesiredAccess,
          uint dwShareMode, uint dwCreationDisposition, ref CREATEFILE2_EXTENDED_PARAMETERS pCreateExParams);

        [StructLayout(LayoutKind.Sequential)]
        public struct CREATEFILE2_EXTENDED_PARAMETERS
        {
            public int dwSize;
            public uint dwFileAttributes;
            public uint dwFileFlags;
            public uint dwSecurityQosFlags;
            public IntPtr lpSecurityAttributes;
            public IntPtr hTemplateFile;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct HIDD_ATTRIBUTES
        {
            public int Size;
            public short VendorID;
            public short ProductID;
            public short VersionNumber;
        }
        #endregion

        #region Constants
        // Wiimote communication constants.
        public const uint FILE_ATTRIBUTE_OVERLAPPED = 0x40000000;
        public const uint FILE_ACCESS_READWRITE = 3;
        public const uint FILE_SHARE_READWRITE = 3;
        public const uint FILE_MODE_OPEN = 3;

        // Nintendo Wii Input Configuration Device.
        private const ushort vendorId = 0x057E;
        private const ushort productId = 0x0306;
        private const ushort usagePage = 0x0001;
        private const ushort usageId = 0x0005;
        #endregion

        #region Private Members
        private WiimoteInputReport wiimoteInputReport = new WiimoteInputReport(new byte[22], new AccelCalibrationInfo());
        private AccelCalibrationInfo accelCalibrationInfo = new AccelCalibrationInfo();
        private SafeFileHandle mHandle;
        private FileStream mStream;
        private bool readAccelCalibration;
        private object inputReportLock = new object();
        #endregion

        #region Public Methods
        public async Task FindWiimote()
        {

            string selector = HidDevice.GetDeviceSelector(usagePage, usageId, vendorId, productId);

            // Enumerate devices using the selector.
            var devices = await DeviceInformation.FindAllAsync(selector).AsTask();

            if (devices.Any())
            {
                // At this point the device is available to communicate with
                // So we can send/receive HID reports from it or 
                // query it for control descriptions.
                Trace.WriteLine("HID devices found: " + devices.Count + "   " + devices[0].Name + "    " + devices[0].Id);
            }
            else
            {
                Trace.WriteLine("HID device not found");
            }
            try
            {
                //mHandle = CreateFile(devices[0].Id, FILE_ACCESS_READWRITE, FILE_SHARE_READWRITE, IntPtr.Zero, FILE_MODE_OPEN, FILE_ATTRIBUTE_OVERLAPPED, IntPtr.Zero);

                CREATEFILE2_EXTENDED_PARAMETERS parameters = new CREATEFILE2_EXTENDED_PARAMETERS();
                parameters.dwSize = Marshal.SizeOf(parameters);
                parameters.dwFileFlags = FILE_ATTRIBUTE_OVERLAPPED;
                mHandle = CreateFile2(devices[0].Id, FILE_ACCESS_READWRITE, FILE_SHARE_READWRITE, FILE_MODE_OPEN, ref parameters);
                Trace.WriteLine(mHandle);
                // Stream to send output reports to the wiimote.
                mStream = new FileStream(mHandle, FileAccess.ReadWrite, 22, true);
                Trace.WriteLine("Did I make it here");
                AccelReportingMode();
                BeginAsyncRead();
                ReadWiimoteCalibration();
            }
            catch ( Exception e)
            {
                Trace.WriteLine(e.ToString());
            }
        }
        public void GetInputReport(out WiimoteInputReport report)
        {
            lock (inputReportLock)
            {
                report = wiimoteInputReport;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Start reading asynchronously from the controller
        /// </summary>
        private void BeginAsyncRead()
        {
            // if the stream is valid and ready
            if (mStream != null && mStream.CanRead)
            {
                // setup the read and the callback
                byte[] buff = new byte[22];
                mStream.BeginRead(buff, 0, 22, new AsyncCallback(OnReadData), buff);
            }
        }

        /// <summary>
        /// Callback when data is ready to be processed
        /// </summary>
        /// <param name="ar">State information for the callback</param>
        private void OnReadData(IAsyncResult ar)
        {
            // grab the byte buffer
            byte[] buff = (byte[])ar.AsyncState;
            try
            {
                // end the current read
                mStream.EndRead(ar);

                if (!readAccelCalibration)
                {
                    lock (inputReportLock)
                    {
                        wiimoteInputReport = new WiimoteInputReport(buff, accelCalibrationInfo);
                    }
                }
                else
                {
                    accelCalibrationInfo.X0 = buff[6];
                    accelCalibrationInfo.Y0 = buff[7];
                    accelCalibrationInfo.Z0 = buff[8];
                    accelCalibrationInfo.XG = buff[10];
                    accelCalibrationInfo.YG = buff[11];
                    accelCalibrationInfo.ZG = buff[12];
                }
                readAccelCalibration = false;
                // start reading again
                BeginAsyncRead();
            }
            catch (OperationCanceledException)
            {
                Trace.WriteLine("OperationCanceledException");
            }
        }
        private void ReadWiimoteCalibration()
        {
            readAccelCalibration = true;
            int address = 0x0016;
            int size = 7;
            byte[] buffer = new byte[size];

            buffer[0] = 0x17;
            buffer[1] = (byte)(((address & 0xff000000) >> 24));
            buffer[2] = (byte)((address & 0x00ff0000) >> 16);
            buffer[3] = (byte)((address & 0x0000ff00) >> 8);
            buffer[4] = (byte)(address & 0x000000ff);
            buffer[5] = (byte)((size & 0xff00) >> 8);
            buffer[6] = (byte)(size & 0xff);
            mStream.Write(buffer, 0, buffer.Length);
        }
        #endregion

        public void WriteData()
        {
            byte[] buffer = new byte[] { 0x11, 0xF0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            mStream.Write(buffer, 0, buffer.Length);
        }
        public void AccelReportingMode()
        {
            byte[] buffer = new byte[] { 0x12, 0x00, 0x31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            mStream.Write(buffer, 0, buffer.Length);
        }
        public void ButtonsReportingMode()
        {
            byte[] buffer = new byte[] { 0x12, 0x00, 0x30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            mStream.Write(buffer, 0, buffer.Length);
        }
    }
}
