﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WiimoteWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Wiimote wiimote = new Wiimote();
        private static bool aPreviousState;
        private static bool bPreviousState;
        private static bool upPreviousState;
        private static bool downPreviousState;
        private static bool leftPreviousState;
        private static bool rightPreviousState;
        private static bool minusPreviousState;
        private static bool plusPreviousState;
        private static bool homePreviousState;
        private static bool onePreviousState;
        private static bool twoPreviousState;
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void connectBtn_Click(object sender, RoutedEventArgs e)
        {
            Thread t = new Thread(ReadWiimoteData);
            t.Start();
        }
        private async void ReadWiimoteData()
        {
            await wiimote.FindWiimote();

            while (true)
            {
                wiimote.GetInputReport(out WiimoteInputReport inputReport);
                CheckAButton(inputReport);
                CheckBButton(inputReport);
                CheckUpButton(inputReport);
                CheckDownButton(inputReport);
                CheckLeftButton(inputReport);
                CheckRightButton(inputReport);
                CheckPlusButton(inputReport);
                CheckMinusButton(inputReport);
                CheckOneButton(inputReport);
                CheckTwoButton(inputReport);
                CheckHomeButton(inputReport);
                Thread.Sleep(10);
            }
        }

        #region Wiimote Checks
        private static void CheckAButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.A);

            if (isPressed && !aPreviousState)
                Trace.WriteLine("A Button Pressed");
            aPreviousState = isPressed;
        }

        private static void CheckBButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.B);

            if (isPressed && !bPreviousState)
                Trace.WriteLine("B Button Pressed");
            bPreviousState = isPressed;
        }

        private static void CheckUpButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Up);

            if (isPressed && !upPreviousState)
                Trace.WriteLine("Up Button Pressed");
            upPreviousState = isPressed;
        }

        private static void CheckDownButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Down);

            if (isPressed && !downPreviousState)
                Trace.WriteLine("Down Button Pressed");
            downPreviousState = isPressed;
        }
        private static void CheckLeftButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Left);

            if (isPressed && !leftPreviousState)
                Trace.WriteLine("Left Button Pressed");
            leftPreviousState = isPressed;
        }
        private static void CheckRightButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Right);

            if (isPressed && !rightPreviousState)
                Trace.WriteLine("Right Button Pressed");
            rightPreviousState = isPressed;
        }
        private static void CheckMinusButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Minus);

            if (isPressed && !minusPreviousState)
                Trace.WriteLine("Minus Button Pressed");
            minusPreviousState = isPressed;
        }

        private static void CheckPlusButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Plus);

            if (isPressed && !plusPreviousState)
                Trace.WriteLine("Plus Button Pressed");
            plusPreviousState = isPressed;
        }

        private static void CheckOneButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.One);

            if (isPressed && !onePreviousState)
                Trace.WriteLine("One Button Pressed");
            onePreviousState = isPressed;
        }

        private static void CheckTwoButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Two);

            if (isPressed && !twoPreviousState)
                Trace.WriteLine("Two Button Pressed");
            twoPreviousState = isPressed;
        }

        private static void CheckHomeButton(WiimoteInputReport inputReport)
        {
            bool isPressed = inputReport.HasFlags(GamePadButtonFlags.Home);

            if (isPressed && !homePreviousState)
                Trace.WriteLine("Home Button Pressed");
            homePreviousState = isPressed;
        }
        #endregion

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
