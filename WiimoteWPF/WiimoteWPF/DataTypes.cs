﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WiimoteWPF
{
    #region Enums
    public enum GamePadButtonFlags
    {
        A,
        B,
        Up,
        Down,
        Left,
        Right,
        Minus,
        Plus,
        Home,
        One,
        Two,
        Released
    }
    #endregion

    public struct AccelCalibrationInfo
    {
        /// <summary>
        /// Zero point of accelerometer
        /// </summary>
        public byte X0, Y0, Z0;
        /// <summary>
        /// Gravity at rest of accelerometer
        /// </summary>
        public byte XG, YG, ZG;
    }
    public struct AccelerometerInfo
    {
        /// <summary>
        /// Zero point of accelerometer
        /// </summary>
        public float X, Y, Z;
    }
}
